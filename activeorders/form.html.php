<?php include_once '../includes/helpers.inc.php'; ?>
<!doctype html>
<html>
<head>

<link rel="shortcut icon" href="/favicon.png">
<link rel="stylesheet" href="../style.css" type="text/css" media="screen">

<title>Edit Order</title>

</head>
<div id="containerAlt">
<body>

<h1>Edit</h1>
<p><a href="?">Back</a></p>
	<table id="gradient-style" summary="Meeting Results">
		<tr>
			<th>PO#</th>
			<th>Customer</th>
			<th>Description</th>
			<th>Colors</th>
			<th>Quantity</th>
			<th>UPC</th>
			<th>Image</th>
			<th>Invoice</th>
			<th>Status</th>
			<th>Options</th>
		</tr>
		<tr valign="top">
			<form action="?editform" method="post">
			<td><?php htmlout($po); ?></td>
			<td><textarea type="text" name="customername" id="customername" rows="1"/><?php htmlout($customername); ?></textarea></td>
			<td><textarea type="text" name="itemdescription" id="itemdescription" rows="2" cols="24"/><?php htmlout($itemdescription); ?></textarea></td>
			<td><textarea type="text" name="colors" id="colors" rows="1" cols="2"/><?php htmlout($colors); ?></textarea></td>
			<td><textarea type="text" name="quantity" id="quantity" rows="1" cols="6"/><?php htmlout($quantity); ?></textarea></td>
			<td><textarea type="text" name="upc" id="upc" rows="1" cols="6"/><?php htmlout($upc); ?></textarea></td>
			<td>
				<select name="imagereceived" id="imagereceived">
				<option value="No">No</option>			
				<option value="Yes">Yes</option>
				<option selected="selected"><?php htmlout($imagereceived); ?></option>
				</select>
			</td>			<td>
				<select name="invoicepaid" id="invoicepaid">
				<option value="Unpaid">Unpaid</option>			
				<option value="Paid">Paid</option>
				<option selected="selected"><?php htmlout($invoicepaid); ?></option>
				</select>
			</td>
			<td>
				<select name="status" id="status">
				<option value="Active">Active</option>			
				<option value="Complete">Complete</option>
				<option selected="selected"><?php htmlout($status); ?></option>
				</select>
			</td>
			<td>
				<input type="hidden" name="po" value="<?php htmlout($po); ?>"/>
				<input type="submit" name="action" value="Update"/>
			</form>
			<form action="?" method="post">
				<input type="hidden" name="po" value="<?php htmlout($po); ?>"/>
				<input type="submit" name="action" value="Add Products"/>
			</form>
			</td>
		</tr>
</table>
</body>
</div>
</html>