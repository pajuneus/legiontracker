<?php include_once '../includes/helpers.inc.php'; ?>
<!doctype html>
<html>
<head>

<link rel="shortcut icon" href="/favicon.png">
<link rel="stylesheet" href="../style.css" type="text/css" media="screen">

<title>Active Orders</title>

</head>
<div id="container">

<div id="header">
	<div id="active"><a href="../activeorders">Active Orders</a></div>
	<div id="not-active"><a href="../completeorders">Complete Orders</a></div>
	<div id="not-active"><a href="../products">Products</a></div>
	<div id="not-active"><a href="../#">Admin</a></div>
</div>

<body>

<table id="gradient-style" summary="Meeting Results">
	<tr>
	<form action="" method="get">
		<th><input type="text" name="spo" id="spo" maxlength="7" size="7"/></th>
		<th><input type="text" name="scustomername" id="scustomername" maxlength="16" size="16"/></th>
		<th><input type="text" name="sitemdescription" id="sitemdescription" maxlength="36" size="36"/></th>
		<th><input type="text" name="scolors" id="scolors" maxlength="3" size="3"/></th>
		<th><input type="text" name="squantity" id="squantity" maxlength="6" size="6"/></th>
		<th><input type="text" name="supc" id="supc" maxlength="6" size="6"/></th>
		<th><input type="text" name="simagereceived" id="simagereceived" maxlength="3" size="3"/></th>
		<th><input type="text" name="sinvoicepaid" id="sinvoicepaid" maxlength="4" size="4"/></th>
		<input type="hidden" name="action" value="search"/>
		<th><input type="submit" value="Search"/><input type="submit" value="Clear"/></a></th>
	</form>
	<form action="?" method="post">
		<th><input type="submit" name="action" value="New Order"/></th>
	</form>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	</tr>
	<tr>
		<th>PO#</th>
		<th>Customer</th>
		<th>Description</th>
		<th>Colors</th>
		<th>Quantity</th>
		<th>UPC</th>
		<th>Image</th>
		<th>Invoice</th>
		<th>Date Modified</th>
		<th>Options</th>
	</tr>
	<tr></tr>
	
	<?php foreach ($aorders as $aorder): ?>
	
	<tr>
		<td><?php bbcodeout($aorder['po']); ?></td>
		<td><?php bbcodeout($aorder['customername']); ?></td>
		<td><?php bbcodeout($aorder['itemdescription']); ?></td>
		<td><?php bbcodeout($aorder['colors']); ?></td>
		<td><?php bbcodeout(($aorder['quantity']).' '); ?></p></td>
		<td><?php bbcodeout($aorder['upc']); ?></td>
		<td><?php bbcodeout($aorder['imagereceived']); ?></td>
		<td><?php bbcodeout($aorder['invoicepaid']); ?></td>
		<td><?php bbcodeout($aorder['datemodified']); ?></td>
		<td>
		<form action="?" method="post">
			<div>
				<input type="hidden" name="po" value="<?php htmlout($aorder['po']); ?>"/>
				<input type="submit" name="action" value="Edit"/>
				<input type="submit" name="action" value="Delete" onClick="return confirm('Are you sure you want to delete this aorder?')"/>
			</div>
		</form>
		</td>
	</tr>
	<?php endforeach; ?>

</table>

</body>
</div>
</html>