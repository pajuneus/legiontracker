<?php
include_once '../includes/magicquotes.inc.php';

// create new record
if (isset($_POST['action']) and $_POST['action'] == 'New Order')
	{
		include '../includes/connect.inc.php';
		
		$now = date("Y-m-d H:i:s");	
		$sql = "INSERT INTO orders SET
		datecreated = '$now'";
		
		if (!mysqli_query($link, $sql))
			{
			$error = 'Error adding new order.';
			include '../includes/error.html.php';
			exit();
			}
					
		header('Location: .');
		exit();
	}

// delete record
if (isset($_POST['action']) and $_POST['action'] == 'Delete')
	{
		include '../includes/connect.inc.php';
	
		$po = mysqli_real_escape_string($link, $_POST['po']);
		$sql = "DELETE FROM orders WHERE po='$po'";
		if (!mysqli_query($link, $sql))
			{
			$error = 'Error deleting record.';
			include '../includes/error.html.php';
			exit();
			}
		header('Location: .');
		exit();
	}

// edit record
if (isset($_POST['action']) and $_POST['action'] == 'Edit')
	{	
		include '../includes/connect.inc.php';
		
		$po = mysqli_real_escape_string($link, $_POST['po']);
		$sql = "SELECT po, customername, itemdescription, colors, quantity, upc, imagereceived, invoicepaid, status
				FROM orders 
				WHERE po='$po'";

		$result = mysqli_query($link, $sql);
		if (!$result)
			{
			$error = 'Error fetching log details.';
			include '../includes/error.html.php';
			exit();
			}
		$row = mysqli_fetch_array($result);
		
		$po = $row['po'];
		$customername = $row['customername'];
		$itemdescription = $row['itemdescription'];
		$colors = $row['colors'];
		$quantity = $row['quantity'];
		$upc = $row['upc'];
		$imagereceived = $row['imagereceived'];
		$invoicepaid = $row['invoicepaid'];
		$status = $row['status'];

		include 'form.html.php';
		exit();
	}
//post update record
if (isset($_GET['editform']))
	{
	include '../includes/connect.inc.php';
	
	$po = mysqli_real_escape_string($link, $_POST['po']);
	$customername = mysqli_real_escape_string($link, $_POST['customername']);
	$itemdescription = mysqli_real_escape_string($link, $_POST['itemdescription']);
	$colors = mysqli_real_escape_string($link, $_POST['colors']);
	$quantity = mysqli_real_escape_string($link, $_POST['quantity']);
	$upc = mysqli_real_escape_string($link, $_POST['upc']);
	$imagereceived = mysqli_real_escape_string($link, $_POST['imagereceived']);
	$invoicepaid = mysqli_real_escape_string($link, $_POST['invoicepaid']);
	$status = mysqli_real_escape_string($link, $_POST['status']);
			
	$sql = "UPDATE orders SET
			customername='$customername',
			itemdescription='$itemdescription',
			colors='$colors',
			quantity='$quantity',
			upc='$upc',
			imagereceived='$imagereceived',
			invoicepaid='$invoicepaid',
			status='$status'
			WHERE po='$po'";
	
	if (!mysqli_query($link, $sql))
		{
			$error = 'Error updating record';
			include '../includes/error.html.php';
			exit();
		}
	
	header('Location: .');
	exit();
	}
/*end edit */

/*add products to orders */
if (isset($_POST['action']) and $_POST['action'] == 'Add Products')
	{
	
	session_start();
	
	include '../includes/connect.inc.php';
	
	$po = mysqli_real_escape_string($link, $_POST['po']);

	$_SESSION['po'] = $po;
	header('Location: ../products');
	exit();
	}

//search
if (isset($_GET['action']) and $_GET['action'] == 'search')
	{
	include '../includes/connect.inc.php';

	// The basic SELECT statement
	$select = 'SELECT po, customername, itemdescription, colors, quantity, upc, imagereceived, invoicepaid, status, datemodified';
	$from =		' FROM orders';
				
	$where = ' WHERE TRUE';
	
	$order = ' ORDER BY po DESC LIMIT 100';
	
	$spo = mysqli_real_escape_string($link, $_GET['spo']);
	if ($spo != '') // Some search text was specified
	{
		$where .= " AND po LIKE '%$spo%'";
	}
	
	$scustomername = mysqli_real_escape_string($link, $_GET['scustomername']);
	if ($scustomername != '') // Some search text was specified
	{
		$where .= " AND customername LIKE '%$scustomername%'";
	}

	$sitemdescription = mysqli_real_escape_string($link, $_GET['sitemdescription']);
	if ($sitemdescription != '') // Some search text was specified
	{
		$where .= " AND itemdescription LIKE '%$sitemdescription%'";
	}
	
	$scolors = mysqli_real_escape_string($link, $_GET['scolors']);
	if ($scolors != '') // Some search text was specified
	{
		$where .= " AND colors LIKE '%$scolors%'";
	}
	
	$squantity = mysqli_real_escape_string($link, $_GET['squantity']);
	if ($squantity != '') // Some search text was specified
	{
		$where .= " AND quantity LIKE '%$squantity%'";
	}
	
	$supc = mysqli_real_escape_string($link, $_GET['supc']);
	if ($supc != '') // Some search text was specified
	{
		$where .= " AND upc LIKE '%$supc%'";
	}

	$simagereceived = mysqli_real_escape_string($link, $_GET['simagereceived']);
	if ($simagereceived != '') // Some search text was specified
	{
		$where .= " AND imagereceived LIKE '%$simagereceived%'";
	}
	
	$sinvoicepaid = mysqli_real_escape_string($link, $_GET['sinvoicepaid']);
	if ($sinvoicepaid != '') // Some search text was specified
	{
		$where .= " AND invoicepaid LIKE '%$sinvoicepaid%'";
	}
	
	$where .= " AND status = 'Active'";
	
	$result = mysqli_query($link, $select . $from . $where . $order);
	if (!$result)
	{
		$error = 'Error fetching orders.';
		include 'includes/error.html.php';
		exit();
	}
	while ($row = mysqli_fetch_array($result))
	{
	$aorders[] = array('po' => $row['po'], 'customername' => $row['customername'], 'itemdescription' => $row['itemdescription'], 'colors' => $row['colors'], 'upc' => $row['upc'], 'imagereceived' => $row['imagereceived'], 'invoicepaid' => $row['invoicepaid'], 'status' => $row['status'], 'quantity' => $row['quantity'], 'datemodified' => $row['datemodified']);
	}
	
	include 'orders.html.php';
	exit();
	}
// gettng started
include '../includes/connect.inc.php';

$result = mysqli_query($link, "SELECT po, customername, itemdescription, colors, quantity, upc, imagereceived, invoicepaid, status, datemodified FROM orders WHERE status = 'Active'");

	if (!$result)
		{
		$error = 'Error fetching Active Orders: ' . mysqli_error($link);
		include '../includes/error.html.php';
		exit();
		}

	while ($row = mysqli_fetch_array($result))
	{
	$aorders[] = array('po' => $row['po'], 'customername' => $row['customername'], 'itemdescription' => $row['itemdescription'], 'colors' => $row['colors'], 'upc' => $row['upc'], 'imagereceived' => $row['imagereceived'], 'invoicepaid' => $row['invoicepaid'], 'status' => $row['status'], 'quantity' => $row['quantity'], 'datemodified' => $row['datemodified']);
	}

include 'orders.html.php';
?>