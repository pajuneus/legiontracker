<?php include_once '../includes/helpers.inc.php'; ?>
<!doctype html>
<html>
<head>

<link rel="shortcut icon" href="/favicon.png">
<link rel="stylesheet" href="../style.css" type="text/css" media="screen">

<title>Edit Product</title>

</head>
<div id="containerAlt">
<body>

<h1>Edit</h1>
<p><a href="?">Back</a></p>
	<table id="gradient-style" summary="Meeting Results">
		<tr>
			<th>Description</th>
			<th>Product Code</th>
			<th>UPC</th>
			<th>Price</th>
			<th>MSRP</th>
			<th>Case Qty.</th>
			<th>Status</th>
			<th>Notes</th>
			<th>Options</th>
		</tr>
		<tr valign="top">
			<form action="?editform" method="post">
			<td><textarea type="text" name="description" id="description" rows="1"/><?php htmlout($description); ?></textarea></td>
			<td><textarea type="text" name="productcode" id="productcode" rows="1"/><?php htmlout($productcode); ?></textarea></td>
			<td><textarea type="text" name="upc" id="upc" rows="1" cols="6"/><?php htmlout($upc); ?></textarea></td>
			<td><textarea type="text" name="price" id="price" rows="1" cols="6"/><?php htmlout($price); ?></textarea></td>
			<td><textarea type="text" name="msrp" id="msrp" rows="1" cols="1"/><?php htmlout($msrp); ?></textarea></td>
			<td><textarea type="text" name="casequantity" id="casequantity" rows="1" cols="6"/><?php htmlout($casequantity); ?></textarea></td>
			<td><textarea type="text" name="status" id="status" rows="1" cols="1"/><?php htmlout($status); ?></textarea></td>
			<td><textarea type="text" name="notes" id="notes" rows="3" cols="12"/><?php htmlout($notes); ?></textarea></td>
			<td>
				<input type="hidden" name="id" value="<?php htmlout($id); ?>"/>
				<input type="submit" name="action" value="Update"/>
			</form>
			</td>
		</tr>
</table>
</body>
</div>
</html>