<?php
include_once '../includes/magicquotes.inc.php';

// create new record
if (isset($_POST['action']) and $_POST['action'] == 'New Product')
	{
		include '../includes/connect.inc.php';
		
		$now = date("Y-m-d H:i:s");	
		$sql = "INSERT INTO products SET
		datecreated = '$now'";
		
		if (!mysqli_query($link, $sql))
			{
			$error = 'Error adding new order.';
			include '../includes/error.html.php';
			exit();
			}
					
		header('Location: .');
		exit();
	}

// delete record
if (isset($_POST['action']) and $_POST['action'] == 'Delete')
	{
		include '../includes/connect.inc.php';
	
		$id = mysqli_real_escape_string($link, $_POST['id']);
		$sql = "DELETE FROM products WHERE id='$id'";
		if (!mysqli_query($link, $sql))
			{
			$error = 'Error deleting record.';
			include '../includes/error.html.php';
			exit();
			}
		header('Location: .');
		exit();
	}

// edit record
if (isset($_POST['action']) and $_POST['action'] == 'Edit')
	{	
		include '../includes/connect.inc.php';
		
		$id = mysqli_real_escape_string($link, $_POST['id']);
		$sql = "SELECT id, description, productcode, upc, price, msrp, casequantity, status, notes
				FROM products 
				WHERE id='$id'";

		$result = mysqli_query($link, $sql);
		if (!$result)
			{
			$error = 'Error fetching log details.';
			include '../includes/error.html.php';
			exit();
			}
		$row = mysqli_fetch_array($result);
		
		$id = $row['id'];
		$description = $row['description'];
		$productcode = $row['productcode'];
		$upc = $row['upc'];
		$price = $row['price'];
		$msrp = $row['msrp'];
		$casequantity = $row['casequantity'];
		$status = $row['status'];
		$notes = $row['notes'];

		include 'form.html.php';
		exit();
	}
//post update record
if (isset($_GET['editform']))
	{
	include '../includes/connect.inc.php';
	
	$id = mysqli_real_escape_string($link, $_POST['id']);
	$description = mysqli_real_escape_string($link, $_POST['description']);
	$productcode = mysqli_real_escape_string($link, $_POST['productcode']);
	$upc = mysqli_real_escape_string($link, $_POST['upc']);
	$price = mysqli_real_escape_string($link, $_POST['price']);
	$msrp = mysqli_real_escape_string($link, $_POST['msrp']);
	$casequantity = mysqli_real_escape_string($link, $_POST['casequantity']);
	$status = mysqli_real_escape_string($link, $_POST['status']);
	$notes = mysqli_real_escape_string($link, $_POST['notes']);
			
	$sql = "UPDATE products SET
			description='$description',
			productcode='$productcode',
			upc='$upc',
			price='$price',
			msrp='$msrp',
			casequantity='$casequantity',
			status='$status',
			notes='$notes'
			WHERE id='$id'";
	
	if (!mysqli_query($link, $sql))
		{
			$error = 'Error updating record';
			include '../includes/error.html.php';
			exit();
		}
	
	header('Location: .');
	exit();
	}
/*end edit */

//search
if (isset($_GET['action']) and $_GET['action'] == 'search')
	{
	include '../includes/connect.inc.php';

	// The basic SELECT statement
	$select = 'SELECT id, description, productcode, upc, price, msrp, casequantity, status, notes';
	$from =		' FROM products';
				
	$where = ' WHERE TRUE';
	
	$order = ' ORDER BY description DESC LIMIT 100';
	
	$sdescription = mysqli_real_escape_string($link, $_GET['sdescription']);
	if ($sdescription != '') // Some search text was specified
	{
		$where .= " AND description LIKE '%$sdescription%'";
	}
	
	$sproductcode = mysqli_real_escape_string($link, $_GET['sproductcode']);
	if ($sproductcode != '') // Some search text was specified
	{
		$where .= " AND productcode LIKE '%$sproductcode%'";
	}

	$supc = mysqli_real_escape_string($link, $_GET['supc']);
	if ($supc != '') // Some search text was specified
	{
		$where .= " AND upc LIKE '%$supc%'";
	}
	
	$sprice = mysqli_real_escape_string($link, $_GET['sprice']);
	if ($sprice != '') // Some search text was specified
	{
		$where .= " AND price LIKE '%$sprice%'";
	}
	
	$smsrp = mysqli_real_escape_string($link, $_GET['smsrp']);
	if ($smsrp != '') // Some search text was specified
	{
		$where .= " AND msrp LIKE '%$smsrp%'";
	}
	
	$scasequantity = mysqli_real_escape_string($link, $_GET['scasequantity']);
	if ($scasequantity != '') // Some search text was specified
	{
		$where .= " AND casequantity LIKE '%$scasequantity%'";
	}

	$sstatus = mysqli_real_escape_string($link, $_GET['sstatus']);
	if ($sstatus != '') // Some search text was specified
	{
		$where .= " AND status LIKE '%$sstatus%'";
	}

	$result = mysqli_query($link, $select . $from . $where . $order);
	if (!$result)
	{
		$error = 'Error fetching products.';
		include 'includes/error.html.php';
		exit();
	}
	while ($row = mysqli_fetch_array($result))
	{
	$products[] = array('id' => $row['id'], 'description' => $row['description'], 'productcode' => $row['productcode'], 'upc' => $row['upc'], 'price' => $row['price'], 'msrp' => $row['msrp'], 'casequantity' => $row['casequantity'], 'status' => $row['status'], 'notes' => $row['notes']);
	}
	
	include 'products.html.php';
	exit();
	}

// gettng started
include '../includes/connect.inc.php';

$result = mysqli_query($link, "SELECT id, description, productcode, upc, price, msrp, casequantity, status, notes FROM products");

	if (!$result)
		{
		$error = 'Error fetching products: ' . mysqli_error($link);
		include '../includes/error.html.php';
		exit();
		}

	while ($row = mysqli_fetch_array($result))
	{
	$products[] = array('id' => $row['id'], 'description' => $row['description'], 'productcode' => $row['productcode'], 'upc' => $row['upc'], 'price' => $row['price'], 'msrp' => $row['msrp'], 'casequantity' => $row['casequantity'], 'status' => $row['status'], 'notes' => $row['notes']);
	}

include 'products.html.php';
?>