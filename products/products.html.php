<?php include_once '../includes/helpers.inc.php'; ?>
<!doctype html>
<html>
<head>

<link rel="shortcut icon" href="/favicon.png">
<link rel="stylesheet" href="../style.css" type="text/css" media="screen">

<title>Products</title>

</head>
<div id="container">

<div id="header">
	<div id="not-active"><a href="../activeorders">Active Orders</a></div>
	<div id="not-active"><a href="../completeorders">Complete Orders</a></div>
	<div id="active"><a href="../products">Products</a></div>
	<div id="not-active"><a href="../#">Admin</a></div>
</div>

<body>

<table id="gradient-style">
	<tr>
	<form action="" method="get">
		<th><input type="text" name="sdescription" id="sdescription" maxlength="18" size="18"/></th>
		<th><input type="text" name="sproductcode" id="sproductcode" maxlength="12" size="12"/></th>
		<th><input type="text" name="supc" id="supc" maxlength="12" size="12"/></th>
		<th><input type="text" name="sprice" id="sprice" maxlength="6" size="6"/></th>
		<th><input type="text" name="smsrp" id="smsrp" maxlength="6" size="6"/></th>
		<th><input type="text" name="scasequantity" id="scasequantity" maxlength="6" size="6"/></th>
		<th><input type="text" name="sstatus" id="sstatus" maxlength="3" size="3"/></th>
		<input type="hidden" name="action" value="search"/>
		<th><input type="submit" value="Search"/><input type="submit" value="Clear"/></a></th>
	</form>
	<form action="?" method="post">
		<th><input type="submit" name="action" value="New Product"/></th>
	</form>
	<th></th>
	<th></th>
	<th></th>
	<th></th>
	</tr>
	<tr>
		<th>Description</th>
		<th>Product Code</th>
		<th>UPC</th>
		<th>Price</th>
		<th>MSRP</th>
		<th>Case Qty.</th>
		<th>Status</th>
		<th>Options</th>
	</tr>
	<tr></tr>
	
	<?php foreach ($products as $product): ?>
	
	<tr>
		<td><?php bbcodeout($product['description']); ?></td>
		<td><?php bbcodeout($product['productcode']); ?></td>
		<td><?php bbcodeout(($product['upc']).' '); ?></p></td>
		<td><?php bbcodeout($product['price']); ?></td>
		<td><?php bbcodeout($product['msrp']); ?></td>
		<td><?php bbcodeout($product['casequantity']); ?></td>
		<td><?php bbcodeout($product['status']); ?></td>
		<td>
		<form action="?" method="post">
			<div>
				<input type="hidden" name="id" value="<?php htmlout($product['id']); ?>"/>
				<input type="submit" name="action" value="Edit"/>
				<input type="submit" name="action" value="Delete" onClick="return confirm('Are you sure you want to delete this product?')"/>
			</div>
		</form>
		</td>
		<td></td>
	</tr>
	<?php endforeach; ?>

</table>
<h1>Job <?php echo ($_SESSION['po']); ?> Contains</h1>
</body>
</div>
</html>